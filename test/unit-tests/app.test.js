const path = require('path');
const fs = require('fs')

test('Check index.html file exists', () => {
    const filePath = path.join(__dirname, "../../app", "index.html")
    expect(fs.existsSync(filePath)).toBeTruthy();
});

test('Check main.js file exists', () => {
    const filePath = path.join(__dirname, "../../app", "main.js")
    expect(fs.existsSync(filePath)).toBeTruthy();
});

test('Check style.css file exists', () => {
    const filePath = path.join(__dirname, "../../app", "style.css")
    expect(fs.existsSync(filePath)).toBeTruthy();
});

test('Check logo.png file exists', () => {
    const filePath = path.join(__dirname, "../../app", "logo.png")
    expect(fs.existsSync(filePath)).toBeTruthy();
});

test('Check bootstrap.min.js file exists', () => {
    const filePath = path.join(__dirname, "../../app", "assets/js/bootstrap.min.js")
    expect(fs.existsSync(filePath)).toBeTruthy();
});

test('Check bootstrap.min.css file exists', () => {
    const filePath = path.join(__dirname, "../../app", "assets/css/bootstrap.min.css")
    expect(fs.existsSync(filePath)).toBeTruthy();
});
  