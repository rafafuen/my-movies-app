import http from 'k6/http';
import { sleep } from 'k6';
import { check } from 'k6';

export const options = {
    stages:[
        {
            duration: '10s',
            target: 10 // starting from 0 to 100
        },
        {
            duration: '30s',
            target: 10
        },
        {
            duration: '10s',
            target: 0 // decreasing from 100 to 0
        }
    ],
    thresholds: {
        http_req_duration: ['p(95)<300'],
        http_req_duration: ['max<2000'],
        http_req_failed: ['rate<0.1'],
        checks: ['rate>=0.99']
    }
}

export function setup() {
    let res = http.get(`${__ENV.APP_MOVIES_URL}`);
    if (res.error) {
        exec.test.abort('Aborting test. Application is DOWN');
    }
}

export default function () {
    //act
    let res = http.get(`${__ENV.APP_MOVIES_URL}`)
    sleep(1)

    //assert
    check(res, {'status is 200': (r) => r.status === 200,});
}