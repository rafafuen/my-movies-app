const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      
    },
    watchForFileChanges: false,
    video: false,
    reporter: 'junit',
    reporterOptions: {
      mochaFile: 'reports/output.[hash].xml',
      toConsole: true,
    },
  }
});
