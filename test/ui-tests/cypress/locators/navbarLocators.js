const navbarLocators = {
    APP_NAME_HEADER: 'h3.navbar-brand',
    TITLE_INPUT: 'input#title',
    DESCRIPTION_INPUT: 'input#description',
    IMG_URL_INPUT: 'input#imgUrl',
    ADD_BUTTON: 'button#addMovieButton',
    SAVE_BUTTON: 'button#saveMovieButton',
    CANCEL_BUTTON: 'button#cancelMovieButton',
}

export default navbarLocators;