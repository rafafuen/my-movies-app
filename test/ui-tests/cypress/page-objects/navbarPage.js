import navbarLocators from '../locators/navbarLocators'

export default class navbarPage{

    static map = {...navbarLocators}

    static load(){
        cy.visit("https://gl-workshop.gitlab.io/gitlab-ci-cd-basics/index.html")
    }

    static typeTitle(title){
        cy.get(this.map.TITLE_INPUT).type(title)
    }

    static typeDescription(description){
        cy.get(this.map.DESCRIPTION_INPUT).type(description)
    }

    static typeImageLink(img_link){
        cy.get(this.map.IMG_URL_INPUT).type(img_link)
    }

    static pressAddButton(){
        cy.get(this.map.ADD_BUTTON).click()
    }

    static pressSaveButton(){
        cy.get(this.map.SAVE_BUTTON).click()
    }

    static pressCancelButton(){
        cy.get(this.map.CANCEL_BUTTON).click()
    }
}