import homeLocators from '../locators/homeLocators'

export default class HomePage{

    static map = {...homeLocators}

    static load(){
        //cy.visit("https://gl-workshop.gitlab.io/gitlab-ci-cd-basics/index.html")
        cy.visit('/index.html')
    }
}
