import navbarPage from '../page-objects/navbarPage'
import HomePage from '../page-objects/HomePage'

describe('CRUD Movies', () => {

  beforeEach(()=> {
    HomePage.load()
  })

  it('Verify error message is displayed when user does not fill required data', () => {
    navbarPage.pressAddButton()
    cy.get(HomePage.map.ERROR_MESSAGE).should('be.visible')
    cy.get(HomePage.map.ERROR_MESSAGE).contains("Warning! Missing Information, please fill all inputs.")
  })

  it('Verify user is able to add new movies', () => {
    navbarPage.typeTitle('Andres')
    navbarPage.typeDescription('Lopez')
    navbarPage.typeImageLink('https://www.imagelink.com')
    //navbarPage.pressAddButton()
  })
})