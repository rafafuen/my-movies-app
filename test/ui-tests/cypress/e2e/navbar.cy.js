import navbarPage from '../page-objects/navbarPage'
import HomePage from '../page-objects/HomePage'

describe('Testing UI Components', () => {

    beforeEach(()=> {
        HomePage.load()
    })

    it('Verify app title is correct', () => {
      cy.get(navbarPage.map.APP_NAME_HEADER).contains('My Movies App')
    })

    it('Verify required inputs are present', () => {
        cy.get(navbarPage.map.TITLE_INPUT).should('be.visible')  
        cy.get(navbarPage.map.DESCRIPTION_INPUT).should('be.visible')
        cy.get(navbarPage.map.IMG_URL_INPUT).should('be.visible')
        cy.get(navbarPage.map.ADD_BUTTON).should('be.visible')
    })

    it('Verify buttons for modification are hidden', () => {
        cy.get(navbarPage.map.SAVE_BUTTON).should('not.be.visible')
        cy.get(navbarPage.map.CANCEL_BUTTON).should('not.be.visible')
    })
  })