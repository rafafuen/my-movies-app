//GET UI ELEMENTS
const moviesWrapper = document.getElementById("movies-wrapper");
const title = document.getElementById("title");
const description = document.getElementById("description");
const imgUrl = document.getElementById("imgUrl");
const addMovieButton = document.getElementById("addMovieButton");
const saveMovieButton = document.getElementById("saveMovieButton");
const cancelMovieButton = document.getElementById("cancelMovieButton");
const errorMessage = document.getElementById('errorMessage')

// DEFINE DATABASE
let moviesData = [];
let currentMovie;

// ADD MOVIE
const addMovie = () => {
  if(missingRequiredData()){ return }

  const uid = new Date().getTime().toString();
  const movieObj = {
    uid: uid,
    title: title.value,
    description: description.value,
    imgUrl: imgUrl.value,
  };

  moviesData.push(movieObj);
  localStorage.setItem("movies", JSON.stringify(moviesData));
  
  createMovieCard(movieObj.uid, movieObj.title, movieObj.description, movieObj.imgUrl);
  clearFormInputs()
};

// EDIT MOVIE
const editMovie = (uid) => {
  enableModification()
  loadDataOnInputs(uid)
  currentMovie = uid
};

// SAVE CHANGES
const saveMovie = () => {
  //validate data
  if(missingRequiredData()){ return }

  // update database
  moviesData.forEach((movie) => {
    if (movie.uid == currentMovie) {
      movie.title = title.value;
      movie.description = description.value;
      movie.imgUrl = imgUrl.value;
    }
  });
  localStorage.setItem("movies", JSON.stringify(moviesData));

  // update html
  updateCardContent()
  clearFormInputs()
  enableAddition()
};

const deleteMovie = (uid) => {
  let confirmDelete = confirm("Are you sure you want to delete this movie?");
  if (!confirmDelete) { return; }

  deleteMovieCard(uid)
  moviesData = moviesData.filter((note) => {return note.uid != uid;});
  localStorage.setItem("movies", JSON.stringify(moviesData));
};

const cancelMovie = () => {
  clearFormInputs()
  enableAddition()
}

// CREATE HTML MOVIE CARD
const createMovieCard = (uid, title, description, imgUrl) => {
  const movie = document.createElement("div");
  movie.className = "col";
  movie.id = uid;
  movie.innerHTML = `
    <div class="card h-100" style="width: 18rem;">
      <img src="${imgUrl}" class="card-img-top" alt="...">
      <div class="card-body">
        <h3 class="card-title">${title}</h3>
        <p class="card-text">${description}</p>
        <div class="card-footer d-flex justify-content-center">
          <a href="#" class="btn btn-outline-primary me-2" onclick="editMovie(${uid})">Modify</a>
          <a href="#" class="btn btn-outline-danger" onclick="deleteMovie(${uid})">Remove</a>
        </div>
      </div>
    </div>
  `;
        
  moviesWrapper.insertBefore(movie, moviesWrapper.firstChild);
};

//UPDATE CARD CONTENT
const updateCardContent = () => {
  const movieCard = document.getElementById(currentMovie);
  const movieTitle = movieCard.querySelector(".card-title");
  const movieDescription = movieCard.querySelector(".card-text");
  const movieImg = movieCard.querySelector(".card-img-top");

  movieTitle.innerText = title.value;
  movieDescription.innerText = description.value;
  movieImg.src = imgUrl.value;
}

//DELETE HTML MOVIE CARD
const deleteMovieCard = (uid) => {
  const note = document.getElementById(uid);
  note.parentNode.removeChild(note);
}

//VALIDATE REQUIRED INPUTS
const missingRequiredData = () => {
  if (title.value.trim().length == 0 || description.value.trim().length == 0 || imgUrl.value.trim().length == 0) {
    errorMessage.classList.remove("d-none");
    return true;
  }else{
    errorMessage.classList.add("d-none");
    return false
  }
}

//LOAD INFORMATION ON INPUTS
const loadDataOnInputs = (uid) => {
  moviesData.forEach((movie) => {
    if (movie.uid == uid) {
      title.value = movie.title;
      description.value = movie.description;
      imgUrl.value = movie.imgUrl
    }
  });
  title.focus();
}

// CLEAR INPUTS
const clearFormInputs = () => {
  imgUrl.value = "";
  title.value = "";
  description.value = "";
}

// SHOW AND HIDE ADD OR SAVE DEPENDS ON ACTION
const enableModification = () => {
  addMovieButton.classList.add("d-none");
  saveMovieButton.classList.remove("d-none");
  cancelMovieButton.classList.remove("d-none");
}

const enableAddition = () => {
  addMovieButton.classList.remove("d-none");
  saveMovieButton.classList.add("d-none");
  cancelMovieButton.classList.add("d-none");
}

//LOAD MOVIES ON RELOAD
window.addEventListener("load", () => {
  moviesData = localStorage.getItem("movies") ? JSON.parse(localStorage.getItem("movies")): [];
  if (moviesData.length == 0){
    moviesData = loadMoviesFromJson()
  }
  moviesData.forEach((movie) => {
    createMovieCard(movie.uid, movie.title, movie.description, movie.imgUrl);
  });
});

//LOAD MOVIES FROM JSON
const loadMoviesFromJson = () => {
  if (!localStorage.getItem("movies")){
    return [
      {
          "uid": "1708391749421",
          "title": "Mimi wo sumaseba",
          "description": "Curious as to who he is, Shizuku meets a boy her age whom she finds infuriating, but discovers to her shock that he is her 'Prince of Books'. As she grows closer to him, she realises that he merely read all those books to bring himself closer to her. The boy Seiji aspires to be a violin maker in Italy",
          "imgUrl": "https://image.tmdb.org/t/p/w600_and_h900_bestv2/5E3Hvbu0bg38ouYf6chGftVGqZ7.jpg"
      },
      {
          "uid": "1708391823852",
          "title": "Porco Rosso",
          "description": "known in Japan as Crimson Pig (Kurenai no Buta) is the sixth animated film by Hayao Miyazaki and released in 1992. You're introduced to an Italian World War I fighter ace, now living as a freelance bounty hunter chasing",
          "imgUrl": "https://image.tmdb.org/t/p/w600_and_h900_bestv2/byKAndF6KQSDpGxp1mTr23jPbYp.jpg"
      },
      {
          "uid": "1708391869076",
          "title": "Spirited Away",
          "description": "is an Oscar winning Japanese animated film about a ten year old girl who wanders away from her parents along a path that leads to a world ruled by strange and unusual monster-like animals",
          "imgUrl": "https://image.tmdb.org/t/p/w600_and_h900_bestv2/39wmItIWsg5sZMyRUHLkWBcuVCM.jpg"
      },
      {
          "uid": "1708391900827",
          "title": "Howl's Moving Castle",
          "description": "When Sophie, a shy young woman, is cursed with an old body by a spiteful witch, her only chance of breaking the spell lies with a self-indulgent yet insecure young wizard and his companions in his legged, walking home",
          "imgUrl": "https://image.tmdb.org/t/p/w600_and_h900_bestv2/TkTPELv4kC3u1lkloush8skOjE.jpg"
      }
    ]
  }
}